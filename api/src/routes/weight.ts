import express from 'express';
import db from '../dbManager';
import { DocumentReference, FieldValue, Timestamp } from 'firebase-admin/firestore';

const router = express.Router();

router.post('/:email', async (req, res) => {
    const email = req.params.email;
    const data = req.body;
    const usersRef = db.collection('users');
    const snapshot = await usersRef.where('email', '==', email).get();
    const userId = data.userId;

    delete data.userId;

    if (!email || !data || !data.weight) {
        res
            .status(400)
            .send({
                error: true,
                message: 'Incomplete request.'
            })
    }

    if (snapshot.empty) {
        res.status(404)
            .send({
                error: true,
                message: 'User not found.'
            })
    }

    snapshot.docs.forEach(async (doc) => {
        const dbUser = doc.data();

        if (dbUser.weightHistory) {
            const userWeight: DocumentReference = await dbUser.weightHistory;

            userWeight.update({
                history: FieldValue.arrayUnion({
                    weight: data.weight,
                    date: Timestamp.fromDate(data.date ? new Date(data.date) : new Date())
                }),
                lastUpdated: FieldValue.serverTimestamp()
            })
                .then((_res) => {
                    res
                        .status(200)
                        .send({
                            error: false,
                            message: "User susccesfully updated."
                        })
                })
                .catch((err) => {
                    res
                        .status(500)
                        .send({
                            error: true,
                            message: err
                        })
                })
        } else {
            const newWeightRef = db.collection('userWeights').doc();

            newWeightRef.set({
                history: ({
                    weight: data.weight,
                    date: Timestamp.fromDate(data.date ? data.date : new Date())
                }),
                dateCreated: Timestamp.fromDate(new Date()),
                lastUpdated: FieldValue.serverTimestamp()
            })
                .then(async (_data) => {
                    const userRef = db.collection('users').doc(userId);

                    await userRef.update({
                        weightHistory: newWeightRef,
                        lastUpdated: FieldValue.serverTimestamp()
                    })
                        .then((_data) => {
                            res.status(200)
                                .send({
                                    error: false,
                                    message: "User susccesfully updated."
                                })
                        })
                        .catch((err) => {
                            console.error(err);
                            res
                                .status(500)
                                .send(err);
                        });
                })
                .catch((err) => {
                    console.error(err);
                    res
                        .status(500)
                        .send(err);
                })

        }
    })
})

router.get('/history/:id', async (req, res) => {
    const userId = req.params.id;
    const usersRef = db.collection('users');

    if (!userId) {
        res
            .status(400)
            .send({
                error: true,
                message: 'Incomplete request.'
            })
    }

    const snapshot = await usersRef.doc(userId).get();

    if (!snapshot.exists) {
        res
            .status(404)
            .send({
                error: true,
                message: 'User not found.'
            })
    }

    const weightSnapshot = await snapshot.data()!.weightHistory.get();
    const history = weightSnapshot.data()!.history;

    history.sort((a: { date: { toDate: () => number; }; }, b: { date: { toDate: () => number; }; }) => {
        return a.date.toDate() - b.date.toDate();
    });

    res
        .status(200)
        .send({
            ...history
        })
})

module.exports = router;
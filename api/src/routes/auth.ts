import jwt from 'jsonwebtoken';
import express from 'express';
import db from '../dbManager';
import { FieldValue, Timestamp } from 'firebase-admin/firestore';

const router = express.Router();

router.post('/login', async (req, res) => {
    const user: { email: string, password: string } = req.body;
    const usersRef = db.collection('users');
    const snapshot = await usersRef.where('email', '==', user.email).get();

    if (!user || !user.email || !user.password) {
        res
            .status(400)
            .send({
                error: true,
                message: 'Incomplete request.'
            })
    }

    if (snapshot.empty) {
        res
            .status(404)
            .send({
                error: true,
                message: 'User not found.'
            })
    } else {
        snapshot.docs.forEach((doc) => {
            const dbUser = doc.data();

            if (user.password === dbUser.password) {
                res
                    .status(200)
                    .send({
                        email: user.email,
                        id: doc.id,
                        token: jwt.sign({ user: user.email }, process.env.JWT_SECRET as string)
                    });
            } else {
                res
                    .status(401)
                    .send({
                        error: true,
                        message: 'Incorrect password.'
                    })
            }
        });
    }
});

router.post('/signup', async (req, res) => {
    const user = {
        ...req.body,
        dateCreated: Timestamp.fromDate(new Date()),
        lastUpdated: FieldValue.serverTimestamp()
    };
    const usersRef = db.collection('users');
    const snapshot = await usersRef.where('email', '==', user.email).get();

    if (!user || !user.email || !user.password || !user.firstName || !user.lastName) {
        res
            .status(400)
            .send({
                error: true,
                message: 'Incomplete request.'
            })
    }

    if (snapshot.empty) {
        const newDocRef = usersRef.doc();

        await newDocRef.set(user)
            .then((_data: any) => {
                res
                    .status(200)
                    .send({
                        error: false,
                        message: 'User successfully created.',
                        data: {
                            email: user.email,
                            id: newDocRef.id,
                            token: jwt.sign({ user: user.email }, process.env.JWT_SECRET as string)
                        }
                    });
            })
            .catch((err: any) => {
                console.error(err);
                res
                    .status(500)
                    .send(err);
            });
    } else {
        res
            .status(409)
            .send({
                error: true,
                message: 'User already exists. Please try logging in instead.'
            })
    }
});

module.exports = router;
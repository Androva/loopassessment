import express, { Application, Request, Response, Router } from "express";
import dotenv from 'dotenv';
import bodyParser from "body-parser";

const app: Application = express();
const port: number = 3001;
const loginRouter: Router = require('./routes/auth');
const weightRouter: Router = require('./routes/weight');

var cors = require('cors');
var corsOptions = {
    origin: 'http://localhost:3000'
};

dotenv.config();

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use('/auth', loginRouter);
app.use('/weight', weightRouter);

app.get("/", (_req: Request, res: Response) => {
    res.send("Initialised");
});

app.listen(port, function () {
    console.log(`App is listening on port ${port}`)
});
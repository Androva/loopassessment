import { initializeApp, cert } from 'firebase-admin/app';
import { getFirestore } from 'firebase-admin/firestore';

const serviceAccount = require('../loopassessment-21f768ac1513.json');

initializeApp({
    credential: cert(serviceAccount)
});

const db = getFirestore();

export default db;
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dbManager_1 = __importDefault(require("../dbManager"));
const firestore_1 = require("firebase-admin/firestore");
const router = express_1.default.Router();
router.post('/:email', async (req, res) => {
    const email = req.params.email;
    const data = req.body;
    const usersRef = dbManager_1.default.collection('users');
    const snapshot = await usersRef.where('email', '==', email).get();
    const userId = data.userId;
    delete data.userId;
    if (!email || !data || !data.weight) {
        res
            .status(400)
            .send({
            error: true,
            message: 'Incomplete request.'
        });
    }
    if (snapshot.empty) {
        res.status(404)
            .send({
            error: true,
            message: 'User not found.'
        });
    }
    snapshot.docs.forEach(async (doc) => {
        const dbUser = doc.data();
        if (dbUser.weightHistory) {
            const userWeight = await dbUser.weightHistory;
            userWeight.update({
                history: firestore_1.FieldValue.arrayUnion({
                    weight: data.weight,
                    date: firestore_1.Timestamp.fromDate(data.date ? new Date(data.date) : new Date())
                }),
                lastUpdated: firestore_1.FieldValue.serverTimestamp()
            })
                .then((_res) => {
                res
                    .status(200)
                    .send({
                    error: false,
                    message: "User susccesfully updated."
                });
            })
                .catch((err) => {
                res
                    .status(500)
                    .send({
                    error: true,
                    message: err
                });
            });
        }
        else {
            const newWeightRef = dbManager_1.default.collection('userWeights').doc();
            newWeightRef.set({
                history: ({
                    weight: data.weight,
                    date: firestore_1.Timestamp.fromDate(data.date ? data.date : new Date())
                }),
                dateCreated: firestore_1.Timestamp.fromDate(new Date()),
                lastUpdated: firestore_1.FieldValue.serverTimestamp()
            })
                .then(async (_data) => {
                const userRef = dbManager_1.default.collection('users').doc(userId);
                await userRef.update({
                    weightHistory: newWeightRef,
                    lastUpdated: firestore_1.FieldValue.serverTimestamp()
                })
                    .then((_data) => {
                    res.status(200)
                        .send({
                        error: false,
                        message: "User susccesfully updated."
                    });
                })
                    .catch((err) => {
                    console.error(err);
                    res
                        .status(500)
                        .send(err);
                });
            })
                .catch((err) => {
                console.error(err);
                res
                    .status(500)
                    .send(err);
            });
        }
    });
});
router.get('/history/:id', async (req, res) => {
    const userId = req.params.id;
    const usersRef = dbManager_1.default.collection('users');
    if (!userId) {
        res
            .status(400)
            .send({
            error: true,
            message: 'Incomplete request.'
        });
    }
    const snapshot = await usersRef.doc(userId).get();
    if (!snapshot.exists) {
        res
            .status(404)
            .send({
            error: true,
            message: 'User not found.'
        });
    }
    const weightSnapshot = await snapshot.data().weightHistory.get();
    const history = weightSnapshot.data().history;
    history.sort((a, b) => {
        return a.date.toDate() - b.date.toDate();
    });
    res
        .status(200)
        .send({
        ...history
    });
});
module.exports = router;

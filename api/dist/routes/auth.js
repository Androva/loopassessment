"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const express_1 = __importDefault(require("express"));
const dbManager_1 = __importDefault(require("../dbManager"));
const firestore_1 = require("firebase-admin/firestore");
const router = express_1.default.Router();
router.post('/login', async (req, res) => {
    const user = req.body;
    const usersRef = dbManager_1.default.collection('users');
    const snapshot = await usersRef.where('email', '==', user.email).get();
    if (!user || !user.email || !user.password) {
        res
            .status(400)
            .send({
            error: true,
            message: 'Incomplete request.'
        });
    }
    if (snapshot.empty) {
        res
            .status(404)
            .send({
            error: true,
            message: 'User not found.'
        });
    }
    else {
        snapshot.docs.forEach((doc) => {
            const dbUser = doc.data();
            if (user.password === dbUser.password) {
                res
                    .status(200)
                    .send({
                    email: user.email,
                    id: doc.id,
                    token: jsonwebtoken_1.default.sign({ user: user.email }, process.env.JWT_SECRET)
                });
            }
            else {
                res
                    .status(401)
                    .send({
                    error: true,
                    message: 'Incorrect password.'
                });
            }
        });
    }
});
router.post('/signup', async (req, res) => {
    const user = {
        ...req.body,
        dateCreated: firestore_1.Timestamp.fromDate(new Date()),
        lastUpdated: firestore_1.FieldValue.serverTimestamp()
    };
    const usersRef = dbManager_1.default.collection('users');
    const snapshot = await usersRef.where('email', '==', user.email).get();
    if (!user || !user.email || !user.password || !user.firstName || !user.lastName) {
        res
            .status(400)
            .send({
            error: true,
            message: 'Incomplete request.'
        });
    }
    if (snapshot.empty) {
        const newDocRef = usersRef.doc();
        await newDocRef.set(user)
            .then((_data) => {
            res
                .status(200)
                .send({
                error: false,
                message: 'User successfully created.',
                data: {
                    email: user.email,
                    id: newDocRef.id,
                    token: jsonwebtoken_1.default.sign({ user: user.email }, process.env.JWT_SECRET)
                }
            });
        })
            .catch((err) => {
            console.error(err);
            res
                .status(500)
                .send(err);
        });
    }
    else {
        res
            .status(409)
            .send({
            error: true,
            message: 'User already exists. Please try logging in instead.'
        });
    }
});
module.exports = router;

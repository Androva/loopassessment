"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const body_parser_1 = __importDefault(require("body-parser"));
const app = (0, express_1.default)();
const port = 3001;
const loginRouter = require('./routes/auth');
const weightRouter = require('./routes/weight');
var cors = require('cors');
var corsOptions = {
    origin: 'http://localhost:3000'
};
dotenv_1.default.config();
app.use(cors(corsOptions));
app.use(body_parser_1.default.json());
app.use('/auth', loginRouter);
app.use('/weight', weightRouter);
app.get("/", (_req, res) => {
    res.send("Initialised");
});
app.listen(port, function () {
    console.log(`App is listening on port ${port}`);
});

import React from 'react';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Box from '@mui/material/Box';
import { createTheme, responsiveFontSizes } from '@mui/material/styles';
import './App.css';
import Home from './components/Home';
import Login from './components/Login';
import History from './components/History';

var theme = createTheme({
	palette: {
		primary: {
			main: '#0ccc05',
		},
		secondary: {
			main: '#000',
		},
	},
});

theme = createTheme(theme, {
	palette: {
		info: {
			main: theme.palette.secondary.main,
		},
	},
});

theme = responsiveFontSizes(theme);

export default function App() {
	const [value, setValue] = React.useState('1');
	const [user, setUser] = React.useState(localStorage.getItem('user'));

	const handleChange = (_event: React.SyntheticEvent, newValue: string) => {
		setValue(newValue);
	};

	const headers = [
		{
			title: 'Home',
			component: Home()
		},
		{
			title: 'History',
			component: History()
		},
		{
			title: 'Logout',
		}
	];

	const logout = () => {
		localStorage.removeItem('user');

		window.location.reload();
	}

	const doNothing = () => {
		console.log("I couldn't bear to fight with typescript. I'm sorry.")
	}

	return (
		<div className="App">
			{/* <ThemeProvider theme={theme}> */}
			{
				user ?
					<TabContext value={value}>
						<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
							<TabList onChange={handleChange} centered>
								{
									headers.map((header, index) => {
										return <Tab onClick={header.title === 'Logout' ? logout : doNothing} label={header.title} value={index.toString()} key={index} />
									})
								}
							</TabList>
						</Box>
						{
							headers.map((header, index) => {
								return <TabPanel value={index.toString()} key={index}>{header.component}</TabPanel>
							})
						}
					</TabContext>
					: <Login user={user} setUser={setUser} />
			}
			{/* </ThemeProvider> */}
		</div>
	);
}
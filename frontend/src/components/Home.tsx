import React, { ReactNode } from 'react';
import { Grid, Typography, Slider, Input, Button, Alert, Snackbar } from '@mui/material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import dayjs from 'dayjs';
import 'dayjs/locale/en-gb';
import axios, { AxiosResponse } from 'axios';

export default function Home(): ReactNode {
    const [sliderValue, setSliderValue] = React.useState<number | string | Array<number | string>>(
        70,
    );
    const [dateValue, setDateValue] = React.useState();
    const [errorMessage, setErrorMessage] = React.useState('');
    const [confirmationMessage, setConfirmationMessage] = React.useState('');
    const [errorOpen, setErrorOpen] = React.useState(false);
    const [confirmationOpen, setConfirmationOpen] = React.useState(false);


    const handleSliderChange = (_event: Event, newValue: number | number[]) => {
        setSliderValue(newValue);
    };

    const handleInputChange = (_event: React.ChangeEvent<HTMLInputElement>) => {
        setSliderValue(_event.target.value === '' ? '' : Number(_event.target.value));
    };

    const handleBlur = () => {
        if (typeof sliderValue === 'number') {
            if (sliderValue < 0) {
                setSliderValue(0);
            } else if (sliderValue > 200) {
                setSliderValue(200);
            }
        }
        else {
            setSliderValue(0);
        }
    }

    const handleErrorClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setErrorOpen(false);
    };

    const handleConfirmationClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setConfirmationOpen(false);
    };

    const handleDateChange = (value: any, context: any) => {
        setDateValue(value.$d);
    }

    const submitWeight = () => {
        const data = {
            weight: sliderValue,
            date: dateValue
        };
        const user = JSON.parse(localStorage.getItem('user')!);

        axios.post(`http://localhost:3001/weight/${user.email}`, data)
            .then((_res: AxiosResponse) => {
                setConfirmationMessage('Entry saved!');
                setConfirmationOpen(true);

                setTimeout(() => {
                    setConfirmationOpen(false);
                }, 3000)
            })
            .catch((_err) => {
                setErrorMessage('Please try again.');
                setErrorOpen(true);

                setTimeout(() => {
                    setErrorOpen(false);
                }, 3000)
            })
    }

    return (
        <Grid container
            spacing={2}
            alignItems="center"
            alignContent="center"
            direction="row"
        >
            {/* Title */}
            <Grid item md={3}></Grid>
            <Grid item md={6}>
                <Typography variant="h4">
                    Record Weight
                </Typography>
            </Grid>
            <Grid item md={3}></Grid>

            {/* Slider Title */}
            <Grid item md={3}>
                <Typography id="weight-slider" gutterBottom>
                    Weight (kg)
                </Typography>
            </Grid>
            {/* Slider */}
            <Grid item md={6}>
                <Slider
                    value={typeof sliderValue === 'number' ? sliderValue : 0}
                    onChange={handleSliderChange}
                    aria-labelledby="weight-slider"
                />
            </Grid>
            {/* Slider Input */}
            <Grid item md={3}>
                <Input
                    value={sliderValue}
                    size="small"
                    onChange={handleInputChange}
                    onBlur={handleBlur}
                    inputProps={{
                        step: 5,
                        min: 0,
                        max: 200,
                        type: 'number',
                        'aria-labelledby': 'weight-slider',
                    }}
                />
            </Grid>

            {/* Date Picker */}
            <Grid item md={3}></Grid>
            <Grid item xs={6}>
                <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale='en-gb'>
                    <DatePicker defaultValue={dayjs(Date.now())} value={dateValue} onChange={handleDateChange} />
                </LocalizationProvider>
            </Grid>
            <Grid item xs={3}></Grid>

            {/* Buttons */}
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
                <Button variant="contained" onClick={submitWeight}>
                    Submit
                </Button>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs></Grid>
            <Snackbar open={errorOpen} autoHideDuration={6000} onClose={handleErrorClose}>
                <Alert severity="error">{errorMessage}</Alert>
            </Snackbar>
            <Snackbar open={confirmationOpen} autoHideDuration={6000} onClose={handleConfirmationClose}>
                <Alert severity="success">{confirmationMessage}</Alert>
            </Snackbar>
        </Grid >
    )
}
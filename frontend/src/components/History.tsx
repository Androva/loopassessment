import React, { ReactNode, useEffect } from 'react';
import { Grid, Typography } from '@mui/material';
import axios from 'axios';
import { LineChart } from '@mui/x-charts/LineChart';

export default function History(): ReactNode {
    const [xAxis, setXAxis] = React.useState([1, 2, 3, 5, 8, 10]);
    const [yAxis, setYAxis] = React.useState([2, 5.5, 2, 8.5, 1.5, 5]);
    const [user, setUser] = React.useState(JSON.parse(localStorage.getItem('user')!));

    useEffect(() => {
        const x: any = [];
        const y: any = [];

        if (JSON.parse(localStorage.getItem('user')!)) {
            setUser(JSON.parse(localStorage.getItem('user')!));

            axios.get(`http://localhost:3001/weight/history/${user.id}`)
                .then((response) => {
                    for (var entry in response.data) {
                        x.push(new Date(response.data[entry].date._seconds * 1000));
                        y.push(response.data[entry].weight)
                    }

                    setXAxis(x);
                    setYAxis(y);
                })
                .catch((err) => {
                    console.error(err);
                })
        }
    }, [])

    return (
        <Grid container
            spacing={2}
            alignItems="center"
            alignContent="center"
            direction="row"
        >
            {/* Title */}
            <Grid item md={3}></Grid>
            <Grid item md={6}>
                <Typography variant="h4">
                    Weight History
                </Typography>
            </Grid>
            <Grid item md={3}></Grid>

            {/* Line Chart */}
            <Grid item md={2}></Grid>
            <Grid item md={8}>
                <LineChart
                    xAxis={[
                        {
                            id: 'Dates',
                            data: xAxis,
                            scaleType: 'time',
                        }
                    ]}
                    series={[
                        {
                            id: 'Weight',
                            data: yAxis,
                        },
                    ]}
                    height={500}
                />
            </Grid>
            <Grid item md={3}></Grid>
        </Grid>
    )
}
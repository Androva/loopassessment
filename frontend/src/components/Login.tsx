import { ReactJSXElement } from "@emotion/react/types/jsx-namespace";
import { Typography, TextField, Grid, Button, Snackbar, Alert, AlertProps } from "@mui/material";
import axios, { AxiosResponse } from "axios";
import { Buffer } from "buffer";
import React from "react";

export default function Login(props: any): ReactJSXElement {
    const [login, setLogin] = React.useState(true);
    const [firstName, setFirstName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [emailValid, setEmailValid] = React.useState(false);
    const [password, setPassword] = React.useState('');
    const [validFields, setValidFields] = React.useState(true);
    const [open, setOpen] = React.useState(false);
    const [errorMessage, setErrorMessage] = React.useState('');
    const emailRegex = new RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/g);

    const handleClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    const firstNameChanged = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setFirstName(event.target.value);
        checkFieldsValid();
    }

    const lastNameChanged = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setLastName(event.target.value);
        checkFieldsValid();
    }

    const emailChanged = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setEmail(event.target.value);
        checkFieldsValid();
    }

    const passwordChanged = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setPassword(event.target.value);
        checkFieldsValid();
    }

    const checkFieldsValid = () => {
        if (!login) {
            if (firstName !== '' && lastName !== '' && email !== '' && password !== '') {
                if (emailRegex.test(email)) {
                    setValidFields(false);
                    setEmailValid(true);
                } else {
                    setValidFields(true);
                    setEmailValid(false);
                }
            } else {
                setValidFields(true);
            }
        } else {
            if (email !== '' && password !== '') {
                if (emailRegex.test(email)) {
                    setValidFields(false);
                    setEmailValid(true);
                } else {
                    setValidFields(true);
                    setEmailValid(false);
                }
                setValidFields(false);
            } else {
                setValidFields(true);
            }
        }
    }

    const showSignUp = () => {
        setLogin(!login);
        checkFieldsValid();
    }

    const submit = (_event: any) => {
        var user: { firstName?: string; lastName?: string; email: string; password: string; } | undefined;

        if (login) {
            user = {
                email: email,
                password: Buffer.from(password).toString('base64')
            }

            axios.post('http://localhost:3001/auth/login', user)
                .then((res: AxiosResponse) => {
                    props.setUser(res.data);
                    localStorage.setItem('user', JSON.stringify(res.data));
                })
                .catch((err) => {
                    if (err.response.status === 404) {
                        setErrorMessage('User not found. Please create an account.');
                    } else if (err.response.status === 401) {
                        setErrorMessage('Incorrect password. Please try again.');
                    } else {
                        setErrorMessage('Error logging in user. Please try again later.')
                    }
                    setOpen(true);

                    setTimeout(() => {
                        setOpen(false);
                    }, 3000)
                })
        } else {
            user = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: Buffer.from(password).toString('base64')
            };

            axios.post('http://localhost:3001/auth/signup', user)
                .then((res: AxiosResponse) => {
                    props.setUser(res.data);
                    localStorage.setItem('user', JSON.stringify(res.data));
                })
                .catch((err) => {
                    if (err.response.status === 409) {
                        setErrorMessage('Email already in use. Please try logging in.');
                    } else {
                        setErrorMessage('Error signing up user. Please try again later.');
                    }
                    setOpen(true);

                    setTimeout(() => {
                        setOpen(false);
                    }, 3000)
                })
        }
    }

    return (
        <Grid container
            spacing={2}
            alignItems="center"
            alignContent="center"
            direction="row"
        >
            {/* Login Header */}
            <Grid item md={12}>
                <Typography variant="h4">
                    {login ? "Login" : "Sign Up"}
                </Typography>
            </Grid>

            {/* Login Input - First Name */}
            {
                login ?
                    <></> :
                    <>
                        <Grid item md={4}></Grid>
                        <Grid item md={4}>
                            <TextField
                                id="firstName"
                                label="First Name"
                                variant="outlined"
                                error={firstName === ""}
                                helperText={firstName === "" ? "Please enter your first name" : ""}
                                onChange={firstNameChanged}
                            />
                        </Grid>
                        <Grid item md={4}></Grid></>
            }

            {/* Login Input - Last Name */}
            {
                login ?
                    <></> :
                    <>
                        <Grid item md={4}></Grid>
                        <Grid item md={4}>
                            <TextField
                                id="lastName"
                                label="Last Name"
                                variant="outlined"
                                error={lastName === ""}
                                helperText={lastName === "" ? "Please enter your last name" : ""}
                                onChange={lastNameChanged}
                            />
                        </Grid>
                        <Grid item md={4}></Grid>
                    </>
            }

            {/* Login Input - Email */}
            <Grid item md={4}></Grid>
            <Grid item md={4}>
                <TextField
                    id="email"
                    label="Email"
                    variant="outlined"
                    type="email"
                    error={(email === "" || !emailValid)}
                    helperText={(email === "" || !emailValid) ? "Please enter your email address" : ""}
                    onChange={emailChanged}
                />
            </Grid>
            <Grid item md={4}></Grid>

            {/* Login Input - Password */}
            <Grid item md={4}></Grid>
            <Grid item md={4}>
                <TextField
                    id="password"
                    label="Password"
                    variant="outlined"
                    type="password"
                    error={password === ""}
                    helperText={password === "" ? "Please enter your password" : ""}
                    onChange={passwordChanged}
                />
            </Grid>
            <Grid item md={4}></Grid>

            {/* Login Buttons */}
            <Grid item xs></Grid>
            <Grid item xs={6}>
                <Button variant="contained" onClick={submit} disabled={validFields}>
                    {login ? 'Login' : 'Sign Up'}
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button variant="outlined" onClick={showSignUp}>
                    {login ? 'Sign Up' : 'Login'}
                </Button>
            </Grid>
            <Grid item xs></Grid>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert severity="error">{errorMessage}</Alert>
            </Snackbar>
        </Grid >
    )
}